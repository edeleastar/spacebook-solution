package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class UserProfile extends Controller
{
  public static void index()
  {
    render();
  }
  
  public static void visit(Long id)
  {
    User user = User.findById(id);
    Logger.info("Just visiting the page for " + user.firstName + ' ' + user.lastName );
    
    List<Message> usermessages = Message.find("byTo", user).fetch();
    render(user, usermessages);
  }
  
  public static void leaveMessage (Long id, String message)
  {
    String userId = session.get("logged_in_userid");
    User fromUser = User.findById(Long.parseLong(userId));
    User toUser = User.findById(id);
    
    Logger.info("Message from user " + 
                 fromUser.firstName + ' ' + fromUser.lastName +" to " +
                 toUser.firstName + ' ' + toUser.lastName +": " +
                 message);
    
    Message messageObj = new Message(fromUser, toUser, message);
    messageObj.save();
    visit(id);
  }
}