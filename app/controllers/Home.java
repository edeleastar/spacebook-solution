package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Home extends Controller
{
  public static void index()
  {
    String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));
    List<Message> usermessages = Message.find("byTo", user).fetch();
    render(user, usermessages);
  }
  
  public static void drop(Long id)
  {
    String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));
    
    User userToDrop = User.findById(id);
    user.following.remove(userToDrop);
    user.save();
    
    Logger.info("Dropping " + userToDrop.email);
    index();
  }
}